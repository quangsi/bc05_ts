console.log("Hello TS");
// Khi báo biến
// Primitive value : string,number,boolean,null, undefined
var username = "alice";
username = "bob";
// username = 3 //error
var age = 2;
// age = "2";
var isHoliday = true;
var isMarried = null;
var is_married = undefined;
var sv1 = {
    id: 1,
    name: "alice",
    age: 20,
};
var todo1 = {
    id: 1,
    title: "Lau nhà",
    isFinished: false,
};
var todo2 = {
    id: 2,
    title: "Làm dự án cúôi khoá",
    // desc: "Không làm thì ko có chứng nhận",
    isFinished: false,
};
var tivi = {
    id: 1,
    name: "Samsung oled tivi",
    price: 10,
};
// type array
var numberArr = [2, 4, 6];
var gioiTinh = ["Nam", "Nữ"];
var danhSachSanPham = [
    tivi,
    {
        id: 2,
        name: "Tủ lạnh",
        price: 5,
    },
];
// type function
function tinhTong(num1, num2) {
    return num1 + num2;
}
function handleClick(value) {
    console.log(value);
}
function main(callback) {
    callback("Hello cybersoft");
}
function renderSection(content) {
    document.getElementById("root").innerHTML = `<h1 style="color:red">${content}</h1>`;
}
main(renderSection);
