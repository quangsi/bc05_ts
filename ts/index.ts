console.log("Hello TS");

// Khi báo biến

// Primitive value : string,number,boolean,null, undefined

var username: string = "alice";
username = "bob";
// username = 3 //error

var age = 2;
// age = "2";
var isHoliday: boolean = true;
var isMarried: null = null;
var is_married: undefined = undefined;

// reference value : object ,array => interface
// interface => dùng để mô trả kiểu dữ liệu của object

interface SinhVien {
  id: number;
  name: string;
  age: number;
}
var sv1: SinhVien = {
  id: 1,
  name: "alice",
  age: 20,
};

interface Todo {
  id: number;
  title: string;
  isFinished: boolean;
}
var todo1: Todo = {
  id: 1,
  title: "Lau nhà",
  isFinished: false,
};

interface NewTodo extends Todo {
  desc?: string; //optinal property
}

var todo2: NewTodo = {
  id: 2,
  title: "Làm dự án cúôi khoá",
  // desc: "Không làm thì ko có chứng nhận",
  isFinished: false,
};

// type : mô tả object , dùng để tạo ra type mới

type Product = {
  id: number;
  name: string;
  price: number;
};

var tivi: Product = {
  id: 1,
  name: "Samsung oled tivi",
  price: 10,
};

// type array

var numberArr: number[] = [2, 4, 6];

var gioiTinh: string[] = ["Nam", "Nữ"];

var danhSachSanPham: Product[] = [
  tivi,
  {
    id: 2,
    name: "Tủ lạnh",
    price: 5,
  },
];

// type function

function tinhTong(num1: number, num2: number): number {
  return num1 + num2;
}
function handleClick(value: number): void {
  console.log(value);
}

function main(callback: (title: string) => void) {
  callback("Hello cybersoft");
}

function renderSection(content: string) {
  document.getElementById(
    "root"
  ).innerHTML = `<h1 style="color:red">${content}</h1>`;
}

main(renderSection);
